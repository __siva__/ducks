export default function validateFields(data) {
  if (!data) {
    return 'Missing data';
  }

  let fieldsToValidate = [
    'place.name',
    'place.country',
    'place.latitude',
    'place.longitude',
    'item',
    'quantity',
    'time',
    'number_of_ducks'
  ];

  let missingFields = fieldsToValidate.filter(field => {
    if (field.includes('.')) {
      let fieldParts = field.split('.');
      let fieldName = fieldParts[0];
      let fieldValue = data[fieldName];
      let fieldKey = fieldParts[1];
      return !fieldValue || !fieldValue[fieldKey];
    }

    return !data[field];
  });

  let validationErrors;
  if (missingFields.length) {
    validationErrors = `The field${missingFields.length > 1 ? 's' : ''} ${missingFields.join(', ')} are required.`;
  }

  return validationErrors;
}
