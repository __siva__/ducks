/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
const encrypt = require('cypress-nextjs-auth0/encrypt');
module.exports = (on, config) => {
  config.env.auth0_admin_username = process.env.AUTH0_ADMIN_USERNAME;
  config.env.auth_username = process.env.AUTH0_USERNAME
  config.env.auth_password = process.env.AUTH0_PASSWORD
  config.env.auth_domain = process.env.REACT_APP_AUTH0_DOMAIN
  config.env.auth_audience = process.env.REACT_APP_AUTH0_AUDIENCE
  config.env.auth_scope = process.env.REACT_APP_AUTH0_SCOPE
  config.env.auth_client_id = process.env.REACT_APP_AUTH0_CLIENTID
  config.env.auth_client_secret = process.env.REACT_APP_AUTH0_CLIENT_SECRET
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('task', { encrypt });
  return config
}
