
describe('Create entry', () => {
  beforeEach(function () {
    cy.login({
      username: Cypress.env('auth0_username'),
      password: Cypress.env('auth0_password')
    });
  });

  it('should validate fields', () => {
    cy.visit('/record');
    cy.contains('Submit').click();
    cy.get('.notification__message').should('have.length', 1);
  });

  it('should select place', () => {
    cy.visit('/record');
    cy.get('[data-label="places"]').click();
    cy.get('[data-label="places"]').type('Chennai');
    cy.get('[data-label^="places-"]').first().click();
  });

  it('should create new items', () => {
    cy.visit('/record');
    cy.get('[data-label="items"] [data-reach-listbox-button]').click();
    cy.contains('New').click();
    cy.get('[data-label="modal"]').should('have.length', 1);
    cy.get('[data-label="primary-btn"]').click();
    cy.get('.notification__message').should('have.length', 1);
    let itemName = `test${Date.now()}`;
    cy.get('[data-label="item-name"]').type(itemName);
    cy.get('[data-label="primary-btn"]').click();
    cy.get('[data-label="items"] [data-reach-listbox-button]').click();
    cy.contains(itemName).should('have.length', 1);
  });

  it('should save data', () => {
    cy.visit('/record');
    cy.get('[data-label="places"]').click();
    cy.get('[data-label="places"]').type('Chennai');
    cy.get('[data-label^="places-"]').first().click();
    cy.get('[data-label="items"] [data-reach-listbox-button]').click();
    cy.get('[aria-labelledby="item"] [data-reach-listbox-option]').first().click();
    cy.get('[data-label="categories"]').click();
    cy.get('[data-reach-listbox-option]').first().click();
    cy.get('[data-label="quantity"]').type(1);
    cy.get('[data-label="ducks"]').type(1);
    cy.get('[data-label="time"]').type('11:11');
    cy.get('[data-label="repeated"').click();
    cy.get('[data-label="submit"]').click();
    cy.get('.notification__content').should('have.length', 1);
  });
});
