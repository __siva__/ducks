describe('Navigation when the user is logged in', () => {
  beforeEach(function () {
    cy.login({
      username: Cypress.env('auth0_username'),
      password: Cypress.env('auth0_password')
    });
  });

  it('should not show login', () => {
    cy.visit('/');

    cy.get('a[href*="/api/auth/login"]').should('have.length', 0);
  });

  it('should not show logout', () => {
    cy.visit('/');

    cy.get('a[href*="/api/auth/logout"]').should('have.length', 1);
  });
});
