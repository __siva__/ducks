describe('Reports', () => {
  beforeEach(function () {
    cy.login({
      username: Cypress.env('auth0_username'),
      password: Cypress.env('auth0_password')
    });
  });

  it('should not show reports', () => {
    cy.visit('/admin/reports/feed-events');

    cy.get('.table-auto').should('have.length', 0);
  });
});
