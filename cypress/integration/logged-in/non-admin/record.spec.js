describe('Create entry', () => {
  beforeEach(function () {
    cy.login({
      username: Cypress.env('auth0_username'),
      password: Cypress.env('auth0_password')
    });
  });

  it('should not show add category to non-admin users', () => {
    cy.visit('/record');
    cy.get('[data-label="categories"] [data-reach-listbox-button]').click();
    cy.get('[data-label="add-new-category"]').should('have.length', 0);
  });
});
