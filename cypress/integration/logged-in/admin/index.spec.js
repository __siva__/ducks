describe('Navigation when the user is not logged in', () => {
  beforeEach(function () {
    cy.login({
      username: Cypress.env('auth0_admin_username'),
      password: Cypress.env('auth0_password')
    });
  });

  it('should show login', () => {
    cy.visit('/');

    cy.get('a[href*="/api/auth/login"]').should('have.length', 0);
  });

  it('should not show logout', () => {
    cy.visit('/');

    cy.get('a[href*="/admin/reports/feed-events"]').should('have.length', 1);
    cy.get('a[href*="/api/auth/logout"]').should('have.length', 1);
  });

  it('should show reports', () => {
    cy.visit('/');

    cy.get('a[href*="/admin/reports/feed-events"]').should('have.length', 1);
  });
});
