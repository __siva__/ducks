describe('Reports', () => {
  beforeEach(function () {
    cy.login({
      username: Cypress.env('auth0_admin_username'),
      password: Cypress.env('auth0_password')
    });
  });

  it('should show reports', () => {
    cy.visit('/admin/reports/feed-events');
    cy.get('.table-auto').should('have.length', 1);
  });

  it('should set filters', () => {
    cy.visit('/admin/reports/feed-events');

    let firstColumn;
    cy.get('.table-auto > tbody > tr > td:first-child').first().then(($firstColumn) => {
      firstColumn = $firstColumn.text();
      cy.get('.table-auto > tbody > tr > td:first-child').first().click();
      cy.url().should('include', encodeURIComponent(firstColumn));
    });
  });

  it('should set sort', () => {
    cy.visit('/admin/reports/feed-events');

    let firstColumn;
    cy.get('.table-auto > thead > tr > th:first-child').first().then(($firstColumn) => {
      firstColumn = $firstColumn.text();
      cy.get('.table-auto > thead > tr > th:first-child').first().click();
      cy.url().should('include', encodeURIComponent(firstColumn));
      cy.url().should('include', 'desc');
      cy.get('.table-auto > thead > tr > th:first-child').first().click();
      cy.url().should('include', 'asc');
    });
  });
});
