describe('Create entry', () => {
  beforeEach(function () {
    cy.login({
      username: Cypress.env('auth0_admin_username'),
      password: Cypress.env('auth0_password')
    });
  });

  it('should show add category to non-admin users', () => {
    cy.visit('/record');
    cy.get('[data-label="categories"] [data-reach-listbox-button]').click();
    cy.get('[data-label="add-new-category"]').should('have.length', 1);
  });

  it('should create new categories', () => {
    cy.visit('/record');
    cy.get('[data-label="categories"] [data-reach-listbox-button]').click();
    cy.contains('New').click();
    cy.get('[data-label="modal"]').should('have.length', 1);
    cy.get('[data-label="primary-btn"]').click();
    cy.get('.notification__message').should('have.length', 1);
    let itemName = `test${Date.now()}`;
    cy.get('[data-label="category-name"]').type(itemName);
    cy.get('[data-label="primary-btn"]').click();
    cy.get('[data-label="categories"] [data-reach-listbox-button]').click();
    cy.contains(itemName).should('have.length', 1);
  });
});
