describe('Navigation when the user is not logged in', () => {
  beforeEach(function () {
    cy.logout();
  });

  it('should show login', () => {
    cy.visit('/');

    cy.get('a[href*="/api/auth/login"]').should('have.length', 1);
  });

  it('should not show logout', () => {
    cy.visit('/');

    cy.get('a[href*="/api/auth/logout"]').should('have.length', 0);
  });
});
