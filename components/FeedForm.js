
import {
  useState
} from 'react';
import useFetch from 'use-http';
import { store as notificationsStore } from 'react-notifications-component';

import FoodCategories from './FoodCategories';
import FoodItems from './FoodItems';
import PlacesSearch from './PlacesSearch';
import validateFields from '../utils/validate-fields';

function FeedForm() {
  let { post, loading, cache } = useFetch('/api');
  let [canSubmitForm, setCanSubmitForm] = useState(true);
  let defaults = {
    category: '',
    item: '',
    quantity: '',
    number_of_ducks: '',
    time: '',
    isRepeated: false,
    place: {
      latitude: null,
      longitude: null,
      country: null,
      name: null
    }
  };
  let [data, setData] = useState(defaults);

  function handleFieldUpdate(field, value) {
    setData(prevData => ({
      ...prevData,
      [field]: value
    }));
  }

  function setTime(event) {
    let time = event.target.value;
    let [hours, minutes] = time.split(':');
    let date = new Date();
    date.setHours(hours);
    date.setMinutes(minutes);
    setData(prevData => ({
      ...prevData,
      time: date.toISOString()
    }));
  }

  function validateAndSubmit() {
    let errors = validateFields(data);
    if (errors) {
      notificationsStore.addNotification({
        message: errors,
        type: 'danger',
        container: 'top-right',
      });

      return;
    }

    submit(data);
  }

  async function submit(data) {
    try {
      let response = await post('/feed-events', { data, cachePolicy: 'no-cache' });
      if (response.data) {
        notificationsStore.addNotification({
          message: 'Good to know you fed your ducks!',
          type: 'success',
          container: 'top-right'
        });
      } else if (response.error) {
        notificationsStore.addNotification({
          message: response.error.title,
          type: 'danger',
          container: 'top-right'
        });
      }
    } catch(err) {
      // log err with Sentry or other providers in production
      notificationsStore.addNotification({
        message: 'Something went wrong at our end.',
        type: 'error',
        container: 'top-right'
      });
    } finally {
      cache.clear();
    }
  }

  async function toggleFormSubmit(canSubmitForm) {
    setCanSubmitForm(!canSubmitForm);
  }

  return (
    <div className="container px-4 m-auto">
      <div className="flex justify-center p-3">
        <div className="w-full max-w-lg">
          <div className="flex">
            <div className="field mr-4">
              <label id="category">Food Category</label>
              <FoodCategories handleFoodCategoryChange={(value) => handleFieldUpdate('category', value)} aria-labelledby="category" />
            </div>

            <div className="field">
              <label id="item">Food Item</label>
              <FoodItems handleFoodItemChange={(value) => handleFieldUpdate('item', value)} category={data.category} aria-labelledby="item" />
            </div>
          </div>

          <div className="field">
            <label htmlFor="quantity">Quantity of Food Item(in grams)</label>
            <input
              className="block w-full"
              type="text"
              placeholder="0"
              id="quantity"
              data-label="quantity"
              onChange={(e) => handleFieldUpdate('quantity', e.target.value)}
            />
          </div>

          <div className="field">
            <label htmlFor="ducks">Number of Ducks</label>
            <input
              className="block w-full"
              type="text"
              placeholder="0"
              id="ducks"
              data-label="ducks"
              onChange={(e) => handleFieldUpdate('number_of_ducks', e.target.value)}
            />
          </div>

          <div className="field">
            <label htmlFor="time">Time</label>
            <input
              className="block w-full"
              type="time"
              placeholder="0"
              id="time"
              data-label="time"
              onChange={setTime}
            />
          </div>

          <div className="field">
            <input
              type="checkbox"
              id="repeated"
              data-label="repeated"
              onChange={(e) => handleFieldUpdate('isRepeated', e.target.checked)}
            />
            <label htmlFor="repeated" className="inline mb-0 ml-2">Schedule daily?</label>
          </div>

          <PlacesSearch handleSelectPlace={(value) => handleFieldUpdate('place', value)} setLoading={toggleFormSubmit} />

          <button data-label="submit" className="btn btn-blue mt-3" onClick={validateAndSubmit} disabled={!canSubmitForm || loading}>Submit</button>
        </div>
      </div>
    </div>
  );
}

export default FeedForm;
