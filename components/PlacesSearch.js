import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
} from '@reach/combobox';

import '@reach/combobox/styles.css';

import usePlacesAutocomplete, {
  getGeocode,
  getLatLng
} from 'use-places-autocomplete';

import { store as notificationsStore } from 'react-notifications-component';

function PlacesSearch(props) {
  let {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    // TODO this doesnt seem to work, need to check documentation again
    // requestOptions: {
    //   location: { lat: () => props.location.lat, lng: () => props.location.lng },
    //   radius: 20 * 1000
    // },
  });

  let handleInput = (e) => {
    props.setLoading && props.setLoading(true);
    props.handleSelectPlace({
      latitude: null,
      longitude: null,
      name: null,
      country: null
    });

    setValue(e.target.value);
  };

  let handleSelect = async (address) => {
    props.setLoading && props.setLoading(true);
    props.handleSelectPlace({
      latitude: null,
      longitude: null,
      name: null,
      country: null
    });

    setValue(address, false);
    clearSuggestions();

    try {
      let results = await getGeocode({ address });
      let { lat, lng } = await getLatLng(results[0]);

      let name = results[0].address_components[0].long_name;
      let country = results[0].address_components.find(a => a.types.includes('country'));
      if (country) {
        country = country.long_name;
      } else {
        // Country seems to be missing in some cases, using the last level of the address for now.
        // My hunch is that this is because of Google trying to avoid regions that are contested.
        // I felt like having a report for country wise data and a report for showing which
        // countries have the most events.
        country = results[0].address_components[results[0].address_components.length - 1].long_name;
      }

      props.handleSelectPlace({ latitude: lat.toString(), longitude: lng.toString(), name, country });
      props.setLoading && props.setLoading(false);
    } catch (error) {
      // log in production using Sentry or the likes
      notificationsStore.addNotification({
        message: 'Something went wrong at our end.',
        type: 'error',
        container: 'top-right'
      });
    }
  };

  return (
    <div className="search">
      <Combobox onSelect={handleSelect}>
        <ComboboxInput
          value={value}
          onChange={handleInput}
          disabled={!ready}
          placeholder="Name of the Park"
          className="w-full max-w-lg"
          data-label="places"
        />
        <ComboboxPopover>
          <ComboboxList>
            {status === "OK" &&
              data.map(({ place_id, description }) => (
                <ComboboxOption data-label={`places-${place_id}`} key={place_id} value={description} />
              ))}
          </ComboboxList>
        </ComboboxPopover>
      </Combobox>
    </div>
  );
}

export default PlacesSearch;
