import {
  useState,
  useCallback
} from 'react';

import useFetch from 'use-http';
import { store as notificationsStore } from 'react-notifications-component';

import Modal from './Modal';

function NewItem(props) {
  let { post, cache } = useFetch('/api');
  let [newItemName, setNewItemName] = useState('');

  let addNewItem = useCallback(async() => {
    if (!newItemName.length) {
      notificationsStore.addNotification({
        message: 'Please enter a name for the new item',
        type: 'danger',
        container: 'top-right',
      });

      return;
    }

    let data = {
      name: newItemName,
      category_id: props.category
    };

    let response = await post('/food-items', {
      data,
      // Doesn't seem to be working, might as well implement a simple fetch hook without all this
      // automagical caching.

      // One example of a bug -> crete an item & try to create another item with the same name.
      // The cached response from the first request is being returned even though the request
      // would have failed had it reached the server.

      // Clearing cache on every request seems to be the best option for now :facepalm:
      cachePolicy: 'no-cache'
    });

    if (response.data) {
      let item = {
        id: response.data.id,
        name: newItemName
      };

      props.onCreate(item);
    } else {
      let { error } = response;
      notificationsStore.addNotification({
        message: error.title,
        type: 'danger',
        container: 'top-right',
      });
    }

    cache.clear();
  }, [post, props, newItemName]);

  function setItemName(event) {
    setNewItemName(event.target.value);
  }

  return (
    <Modal
      title="Add New Food Item"
      description="Enter the name of the food item you would like to add."
      primaryButtonTitle="Add"
      primaryButtonAction={addNewItem}
      closeModal={props.onClose}
    >
       <input data-label="item-name" className="mt-2" type="text" onChange={setItemName} />
    </Modal>
  );
}

export default NewItem;
