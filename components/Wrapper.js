import Link from 'next/link';
import Image from 'next/image';
import { useUser } from '@auth0/nextjs-auth0';
import { useState, useEffect } from 'react';
import useFetch from 'use-http';

function Wrapper(props) {
  let { user } = useUser();
  let { get } = useFetch('/api');

  let [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    get('/is-admin').then(response => {
      if (response.data) {
        setIsAdmin(response.data.is_admin);
      }
    });
  }, []);

  return (
    <>
      <nav className="h-16 bg-blue-100 flex items-center justify-between px-5">
        <div>
          <Link href="/">
            <a className="text-xl">
              <Image
                src="/duck.png"
                alt="A yellow duck flying"
                width={50}
                height={50}
              />
              <span className="ml-2 inline-block" style={{verticalAlign: 'top', marginTop: '13px'}}>Ducks</span>
            </a>
          </Link>
        </div>
        {user && (
          <div>
            {isAdmin && (
              <Link href="/admin/reports/feed-events">
                <a className="mr-5">Reports</a>
              </Link>
            )}

            <a href="/api/auth/logout">Logout</a>
          </div>
        )}
      </nav>
      {props.children}
    </>
  );
}

export default Wrapper;
