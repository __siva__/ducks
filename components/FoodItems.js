import {
  useState,
  useEffect
} from 'react';

import {
  Listbox,
  ListboxOption,
} from '@reach/listbox';

import useFetch from 'use-http';

import '@reach/listbox/styles.css';

import NewFoodItem from './NewFoodItem';

function FoodItems(props) {
  let { get, loading } = useFetch('/api');

  let [foodItems, setFoodItems] = useState([]);
  let [foodItem, setFoodItem] = useState(props.foodItem || '');

  let [canShowAddNew, setCanShowAddNew] = useState(false);

  // load food items when cateogry changes
  useEffect(() => {
    if (props.category) {
      get(`/food-items?category=${props.category}`).then(response => {
        if (response.data) {
          setFoodItems(response.data);
          handleChange(response.data[0].id);
        } else {
          setFoodItems([]);
          setFoodItem('');
        }
      });
    }
  }, [get, setFoodItems, props.category]);

  function handleChange(item) {
    setFoodItem(item);
    props.handleFoodItemChange(item);
  }

  function onItemCreate(item) {
    setFoodItems([...foodItems, item]);
    handleChange(item.id);
    setCanShowAddNew(false);
  }

  if (loading) {
    return (
      <Listbox
        aria-labelledby={props['aria-labelledby']}
        onChange={handleChange}
        value="Loading"
        className="capitalize"
        disabled={true}
      >
        <ListboxOption key="Loading" value="Loading" className="capitalize">
          Loading
        </ListboxOption>
        <p
          role="button"
          onClick={() => setCanShowAddNew(true)}
          key={'add-new'}
          className="px-2 border-t-2 border-solid border-gray-300 text-indigo-800"
        >
          + New
        </p>
      </Listbox>
    );
  }

  return (
    <>
      {
        (
          <Listbox
            aria-labelledby={props['aria-labelledby']}
            onChange={handleChange}
            value={foodItem}
            className="capitalize"
            data-label="items"
          >
            {foodItems.map(item => (
              <ListboxOption data-label="food-item" key={item.id} value={item.id} className="capitalize">
                {item.name}
              </ListboxOption>
            ))}
            <p
              role="button"
              onClick={() => setCanShowAddNew(true)}
              key={'add-new'}
              className="px-2 border-t-2 border-solid border-gray-300 text-indigo-800"
            >
              + New
            </p>
          </Listbox>
        )
      }

      {canShowAddNew && (
        <NewFoodItem
          category={props.category}
          onCreate={onItemCreate}
          onClose={() => setCanShowAddNew(false)}
        />
      )}
    </>
  )
}

export default FoodItems;
