import {
  useState,
  useCallback
} from 'react';

import useFetch from 'use-http';
import { store as notificationsStore } from 'react-notifications-component';

import Modal from './Modal';

function NewFoodCategory(props) {
  let { post, cache } = useFetch('/api');
  let [newCategory, setNewCategory] = useState('');

  let addNewCategory = useCallback(async() => {
    if (!newCategory.length) {
      notificationsStore.addNotification({
        message: 'Please enter a name for the new category',
        type: 'danger',
        container: 'top-right',
      });

      return;
    }

    let data = {
      name: newCategory,
      category_id: props.category
    };

    let response = await post('/food-categories', {
      data,
      // Doesn't seem to be working, might as well implement a simple fetch hook without all this
      // automagical caching.

      // One example of a bug -> crete an item & try to create another item with the same name.
      // The cached response from the first request is being returned even though the request
      // would have failed had it reached the server.

      // Clearing cache on every request seems to be the best option for now :facepalm:
      cachePolicy: 'no-cache'
    });

    if (response.data) {
      let item = {
        id: response.data.id,
        name: newCategory
      };

      props.onCreate(item);
    } else {
      let { error } = response;
      notificationsStore.addNotification({
        message: error.title,
        type: 'danger',
        container: 'top-right',
      });
    }

    cache.clear();
  }, [post, props, newCategory]);

  function setCategory(event) {
    setNewCategory(event.target.value);
  }

  return (
    <Modal
      title="Add New Food Category"
      description="Enter the name of the food category you would like to add."
      primaryButtonTitle="Add"
      primaryButtonAction={addNewCategory}
      closeModal={props.onClose}
    >
       <input data-label="category-name" className="mt-2" type="text" onChange={setCategory} />
    </Modal>
  );
}

export default NewFoodCategory;
