import {
  useState,
  useEffect
} from 'react';

import useFetch from 'use-http';

import {
  Listbox,
  ListboxOption,
} from '@reach/listbox';

import '@reach/listbox/styles.css';

import NewFoodCategory from './NewFoodCategory';

function FoodCategories(props) {
  let { get, loading, error } = useFetch('/api');

  let [foodCategories, setFoodCategories] = useState([]);
  let [foodCategory, setFoodCategory] = useState(props.foodCategory || '');
  let [isUserAllowedToCreate, setIsUserAllowedToCreate] = useState(false);
  let [canShowCreateForm, setCanShowCreateForm] = useState(false);

  useEffect(() => {
    get('/food-categories').then(response => {
      if (response.data) {
        setFoodCategories(response.data);
        handleChange(response.data[0].id);
      }
    });

    get('/is-admin').then(response => {
      if (response.data) {
        setIsUserAllowedToCreate(response.data.is_admin);
      }
    });
  }, []);

  function onCategoryCreate(category) {
    setFoodCategories([...foodCategories, category]);
    handleChange(category.id);
    setCanShowCreateForm(false);
  }

  function handleChange(category) {
    setFoodCategory(category);
    props.handleFoodCategoryChange(category);
  }

  if (loading) {
    return (
      <Listbox
        aria-labelledby={props['aria-labelledby']}
        onChange={handleChange}
        value="Loading"
        className="capitalize"
        data-label="categories"
        disabled={true}
      >
        <ListboxOption data-label="food-category" key="category-loading" value="Loading" className="capitalize">
          Loading
        </ListboxOption>

        {
          isUserAllowedToCreate &&
          <p
            role="button"
            onClick={() => setCanShowCreateForm(true)}
            key={'add-new'}
            data-label="add-new-category"
            className="px-2 border-t-2 border-solid border-gray-300 text-indigo-800"
          >
            + New
          </p>
        }
      </Listbox>
    );
  }

  return (
    <>
      {error && 'Error!'}

      {
        error ?
          <p>{error.message}</p> :
          <Listbox
            aria-labelledby={props['aria-labelledby']}
            onChange={handleChange}
            value={foodCategory}
            className="capitalize"
            data-label="categories"
          >
            {foodCategories.map(category => (
              <ListboxOption data-label="food-category" key={category.id} value={category.id} className="capitalize">
                {category.name}
              </ListboxOption>
            ))}

            {
              isUserAllowedToCreate &&
              <p
                role="button"
                onClick={() => setCanShowCreateForm(true)}
                key={'add-new'}
                data-label="add-new-category"
                className="px-2 border-t-2 border-solid border-gray-300 text-indigo-800"
              >
                + New
              </p>
            }
          </Listbox>
      }

      {canShowCreateForm && (
        <NewFoodCategory
          category={props.category}
          onCreate={onCategoryCreate}
          onClose={() => setCanShowCreateForm(false)}
        />
      )}
    </>
  )
}

export default FoodCategories;
