import {
  useState,
  useEffect,
  useMemo,
  useCallback
} from 'react';

import Head from 'next/head';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { withPageAuthRequired } from '@auth0/nextjs-auth0';
import { DateTime } from 'luxon';

import useFetch from 'use-http';

import Loading from '../../../components/Loading';

export default withPageAuthRequired(function Reports() {
  let router = useRouter();
  let [events, setEvents] = useState([]);
  let [sortColumns, setSortColumns] = useState({});
  let { get, loading, error, cache } = useFetch('/api/admin/reports');

  useEffect(() => {
    let queryParams = router.query;
    let encodedQueryParams = Object.keys(queryParams).map(key => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(queryParams[key]);
    }).join('&');
    encodedQueryParams = encodedQueryParams.length ? '?' + encodedQueryParams : '';

    get(`/feed-events${encodedQueryParams}`).then(response => {
      if (response.data) {
        let formattedEvents = response.data.map(event => {
          return {
            ...event,
            time: DateTime.fromISO(event.time).toLocaleString(DateTime.DATETIME_MED)
          }
        });

        setEvents(formattedEvents);

        cache.clear();
      }
    });
  }, [get, router, cache]);

  let tableHeaders = useMemo(() => {
    return events.length > 0 ? Object.keys(events[0]).filter(key => key !== 'id') : []
  }, [events]);

  useEffect(() => {
    let newSortColumns = {};

    tableHeaders.forEach(header => {
      newSortColumns[header] = {
        direction: 'asc',
        active: false
      }
    });

    setSortColumns(sortColumns => {
      // This is intentional to avoid overwriting the previous sort state
      return {
        ...newSortColumns,
        ...sortColumns,
        filter_by: sortColumns.filter_by || '',
        filter_value: sortColumns.filter_value || ''
      }
    });
  }, [tableHeaders])

  useEffect(() => {
    let activeColumn = Object.keys(sortColumns).find(key => sortColumns[key].active);
    if (activeColumn) {
      let sortColumn = activeColumn;
      let sortOrder = sortColumns[activeColumn].direction;
      let queryParams = router.query;

      if (queryParams.sort_column !== sortColumn || queryParams.sort_order !== sortOrder) {
        let newQps = {
          sort_column: sortColumn,
          sort_order: sortOrder
        };

        if (queryParams.filter_by) {
          newQps.filter_by = queryParams.filter_by;
        }

        if (queryParams.filter_value) {
          newQps.filter_value = queryParams.filter_value;
        }

        router.push({
          pathname: router.pathname,
          query: newQps
        });
      }
    }
  }, [sortColumns, router]);

  let downloadCSV = useCallback(() => {
    let csv = [tableHeaders.join(',')].concat(events.map(event => {
      return tableHeaders.map(key => {
        return event[key].toString().replace(/,/g, ' ');
      }).join(',');
    })).join('\n');

    csv = 'data:text/csv;charset=utf-8,' + csv;

    let encodedUri = encodeURI(csv);
    let link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', 'feed-events.csv');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }, [events, tableHeaders]);

  let sortBy = useCallback(
    (column) => {
      let newSortColumns = { ...sortColumns };
      Object.keys(newSortColumns).forEach(key => {
        if (typeof newSortColumns[key] === 'object') {
          newSortColumns[key].active = false;
        }
      });

      let newDirection = newSortColumns[column].direction === 'asc' ? 'desc' : 'asc';
  
      newSortColumns[column].direction = newDirection;
      newSortColumns[column].active = true;

      setSortColumns(newSortColumns);
    },
    [sortColumns]
  );

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <p>Error: {error.message}</p>;
  }

  let availableDrillDownReports = [
    'person',
    'location',
    'item'
  ];

  return (
    <div className="container m-auto px-4 mt-5 overflow-y-scroll">
      <Head>
        <title>List of duck feeding events</title>
      </Head>

      <table className="table-auto">
        <thead>
          <tr>
            {tableHeaders.map(header => (
              <th className="px-4 py-2 capitalize" key={header} role="button" onClick={() => sortBy(header)}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {events.map(event => (
            <tr key={event.id}>
              {tableHeaders.map(header => (
                <td className="border px-4 py-2" key={`${header}-${event[header]}`}>
                  {availableDrillDownReports.includes(header) ? (
                    <Link href={`${router.pathname}?filter_by=${header}&filter_value=${event[header]}`}>
                      <a className="font-normal">{event[header]}</a>
                    </Link>
                  ) : (
                    <span>{event[header]}</span>
                  )}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>

      <button className="btn btn-blue mt-5" onClick={downloadCSV}>Download as CSV</button>

      <Link href="/admin/reports/feed-events">
        <a>
          <span className="ml-2">Clear Filters</span>
        </a>
      </Link>
    </div>
  );
});
