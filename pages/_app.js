import '../styles/globals.css';
import 'react-notifications-component/dist/theme.css'

import ReactNotification from 'react-notifications-component';
import { UserProvider } from '@auth0/nextjs-auth0';
import Wrapper from '../components/Wrapper';

function MyApp({ Component, pageProps }) {
  return (
    <UserProvider>
      <Wrapper>
        <ReactNotification />
        <Component {...pageProps} />
      </Wrapper>
    </UserProvider>
  );
}

export default MyApp;
