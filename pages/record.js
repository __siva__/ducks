import Head from 'next/head';
import { withPageAuthRequired } from '@auth0/nextjs-auth0';

import FeedForm from '../components/FeedForm';

export default withPageAuthRequired(function Record() {
  return (
    <>
      <Head>
        <title>Save your duck feeding data</title>
      </Head>

      <FeedForm />
    </>
  );
});
