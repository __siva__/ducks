import { withApiAuthRequired, getSession } from '@auth0/nextjs-auth0';

// Ideally, this should be based on roles provided using auth0.
// For some reason, I am not able to get the roles sent along with response
// and since I don't want to spend too much time on it, settling for this.
function isAdmin(email) {
  // let domain = email.split('@')[1];
  return process.env.ADMIN_EMAILS.split('|').includes(email);
}

export default withApiAuthRequired(async function handler(req, res) {
  if (req.method === 'GET') {
    let session = await getSession(req, res);
    let user = session.user;
    if (user) {
      let email = user.email;
      let isUserAnAdmin = isAdmin(email);
      res.status(200).json({
        data: {
          is_admin: isUserAnAdmin
        }
      });

      res.end();
    } else {
      res.statusCode = 401;
      res.end(JSON.stringify({
        error: 'Unauthorized'
      }));
    }
  }

  if (req.method === 'POST') {
    res.status(405).json({
      error: {
        title: 'This endpoint is not available for POST requests.'
      }
    });

    res.end();
  } 
});
