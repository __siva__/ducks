import { query } from '../_lib/db';

class Location {
  async find({ name, country, latitude, longitude }) {
    let location = await query('SELECT * FROM locations WHERE name = ? AND country = ? AND latitude = ? AND longitude = ?', [name, country, latitude, longitude]);
    let response = {};
    if (location.length) {
      response.data = {
        id: location[0].id.toString()
      };
    } else {
      response.error = {
        title: 'Location not found'
      }
    }

    return response;
  }

  async create(data) {
    let newLocation = await query('INSERT INTO locations SET ?', data);
    let response = {
      data: {
        id: newLocation.insertId.toString()
      }
    };

    return response;
  }
}

export default Location;
