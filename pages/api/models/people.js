import { query } from '../_lib/db';

class Person {
  async find(email) {
    let person = await query('SELECT id FROM people WHERE email = ?', [email]);
    let response = {};
    if (person.length) {
      response.data = {
        id: person[0].id.toString()
      }
    } else {
      response.error = {
        title: 'Person not found'
      }
    }

    return response;
  }

  async create({ name, email }) {
    let newPerson = await query('INSERT INTO people (name, email) VALUES (?, ?)', [name, email]);
    let response = {
      data: {
        id: newPerson.insertId.toString()
      }
    };

    return response;
  }
}

export default Person;
