import { query } from '../_lib/db';

class FoodItems {
  async findItemsForCategory(category) {
    let results = await query('SELECT * FROM food_items WHERE category_id = ?', [category]);
    let response = {};
    if (results.length) {
      response.data = results.map(result => ({...result, id: result.id.toString()}))
    } else {
      response.error = {
        title: 'No food items found'
      }
    }

    return response;
  }

  async create(data) {
    let response = {};

    let item = await query('SELECT * FROM food_items WHERE category_id = ? AND name = ?', [data.category_id, data.name]);
    if (item.length) {
      response.code = 409;
      response.error = {
        title: 'Item already exists',
      }

      return response;
    }

    let results = await query('INSERT INTO food_items SET ?', data);
    response = {
      data: {
        id: results.insertId.toString()
      }
    };

    return response;
  }
}

export default FoodItems;
