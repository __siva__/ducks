import { query } from '../_lib/db';

class FoodCategories {
  async findAll() {
    let results = await query('SELECT * FROM food_categories');
    let response = {};
    if (results.length) {
      response.data = results.map(result => ({...result, id: result.id.toString()}));
    } else {
      response.error = {
        title: 'No food categories found',
      };
    }

    return response;
  }

  async create(data) {
    let response = {};
    let category = await query('SELECT * FROM food_categories WHERE name = ?', [data.name]);
    if (category.length) {
      response.code = 409;
      response.error = {
        title: 'Category already exists',
      }

      return response;
    }

    let results = await query('INSERT INTO food_categories SET ?', data);
    response = {
      data: {
        id: results.insertId.toString()
      }
    };

    return response;
  }
}

export default FoodCategories;
