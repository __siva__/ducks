import { DateTime } from 'luxon';
import { getSession } from '@auth0/nextjs-auth0';

import { query, pool } from '../_lib/db';
import validateFields from '../../../utils/validate-fields';

class FeedEvents {
  orderColumnMap = {
    location: 'location',
    person: 'person',
    item: 'item',
    quantity: 'quantity',
    time: 'time',
    [encodeURIComponent('Number of Ducks')]: 'f.number_of_ducks',
  };

  filterColumnMap = {
    location: 'l.name',
    person: 'p.name',
    item: 'i.name',
  };

  async findAll(queryParams) {
    let sortColumn = queryParams.sort_column;
    let orderByColumn = this.orderColumnMap[sortColumn];
    let sortOrder = queryParams.sort_order === 'asc' ? 'ASC' : 'DESC';
    let orderBy = orderByColumn ? `ORDER BY ${orderByColumn} ${sortOrder}` : '';
    let filterBy = queryParams.filter_by;
    // escape filter value as user input is directly passed to MySQL
    let filterValue = pool.escape(queryParams.filter_value);
    let filterByQuery = filterBy && filterValue !== null ? `WHERE ${this.filterColumnMap[filterBy]} = ${(filterValue)}` : '';

    let selectQuery = `
      SELECT
        l.name as "location",
        p.name as "person",
        i.name as "item",
        f.id,
        f.qty as "quantity",
        f.time as "time",
        f.number_of_ducks as "Number of Ducks"
      FROM feed_events AS f
      INNER JOIN locations AS l ON f.location = l.id
      INNER JOIN people AS p on f.person = p.id
      INNER JOIN food_items AS i on f.food_item = i.id
      ${filterByQuery}
      ${orderBy}
    `;

    let results = await query(selectQuery) || [];
    let response = {
      data: results.map(result => ({...result, id: result.id.toString()}))
    }

    return response;
  }

  validateFields(data) {
    return validateFields(data);
  }

  // TODO
  // This should probably be a transaction with rollback and commit.
  // Need to check
  async create(req, res) {
    let session = await getSession(req, res);
    let user = session.user;

    let domain = process.env.NEXT_PUBLIC_VERCEL_URL ? `https://${process.env.NEXT_PUBLIC_VERCEL_URL}` : 'http://localhost:3010';
    let personResponse = await fetch(`${domain}/api/people?email=${user.email}`, {
      headers: {
        cookie: req.headers.cookie
      }
    });

    let personId;
    if (personResponse.status === 404) {
      let newPerson = await fetch(`${domain}/api/people`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          cookie: req.headers.cookie
        },
        body: JSON.stringify({
          data: {
            name: user.name,
            email: user.email
          }
        })
      });
      let { data } = await newPerson.json();
      personId = data.id;
    } else {
      let { data } = await personResponse.json();
      personId = data.id;
    }

    let { data } = req.body;
    let response = {};
    let validationErrors = this.validateFields(data);
    if (validationErrors) {
      response.error = {
        title: validationErrors,
      };

      return response;
    }

    // location
    let place = data.place;
    let params = {
      name: place.name,
      country: place.country,
      latitude: place.latitude,
      longitude: place.longitude
    };

    let locationResponse = await fetch(`${domain}/api/locations?${new URLSearchParams(params).toString()}`, {
      headers: {
        cookie: req.headers.cookie
      }
    });

    let locationId;
    if (locationResponse.status === 404) {
      let newLocation = await fetch(`${domain}/api/locations`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          cookie: req.headers.cookie
        },
        body: JSON.stringify({
          data: {
            ...place
          }
        })
      });

      let { data } = await newLocation.json();
      locationId = data.id;
    } else {
      let { data } = await locationResponse.json();
      locationId = data.id;
    }

    // time
    let dateForMySQL = DateTime.fromISO(data.time).toSQL();

    let feedData = {
      location: locationId,
      person: personId,
      food_item: data.item,
      qty: data.quantity,
      time: dateForMySQL,
      number_of_ducks: data.number_of_ducks,
    };

    let newFeedEvent = await query('INSERT INTO feed_events SET ?', feedData);

    if (data.isRepeated) {
      let eventData = {...feedData};
      delete eventData.time;

      await query(`CREATE EVENT daily_${newFeedEvent.insertId} ON SCHEDULE EVERY 1 DAY STARTS CURRENT_TIMESTAMP + INTERVAL 1 DAY DO INSERT INTO feed_events SET ?`, eventData);
    }

    response.data = {
      id: newFeedEvent.insertId.toString()
    };

    return response;
  }
}

export default FeedEvents;
