import { withApiAuthRequired } from '@auth0/nextjs-auth0';

import handleResponse from './_lib/handle-response';
import FoodItems from './models/food-items';
const foodItems = new FoodItems();

export default withApiAuthRequired(async function handler(req, res) {
  if (req.method === 'GET') {
    return handleResponse(foodItems.findItemsForCategory(req.query.category), res);
  }

  if (req.method === 'POST') {
    return handleResponse(foodItems.create(req.body.data), res);
  }
});
