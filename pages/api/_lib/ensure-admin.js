import handleResponse from './handle-response';

const ensureAdmin = (handler) => {
  return async(req, res) => {
    let domain = process.env.NEXT_PUBLIC_VERCEL_URL ? `https://${process.env.NEXT_PUBLIC_VERCEL_URL}` : 'http://localhost:3010';
    let isAdminCheckResponse = await fetch(`${domain}/api/is-admin`, {
      headers: {
        cookie: req.headers.cookie
      }
    });

    let notAuthorizedResponse = Promise.resolve({
      status: 401,
      error: {
        title: 'You are not authorized to perform this action.',
      }
    });

    if (isAdminCheckResponse.status === 200) {
      let { data } = await isAdminCheckResponse.json();
      if (data.is_admin) {
        return handler(req, res);
      }


      return handleResponse(notAuthorizedResponse, res);
    }

    return handleResponse(notAuthorizedResponse, res);
  };
};

export default ensureAdmin;
