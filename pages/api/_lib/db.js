import mysql from 'mysql';
import { promisify } from 'util';

import dbConfig from './db.config';

const pool = mysql.createPool({
  connectionLimit: 10,
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
});

const query = promisify(pool.query).bind(pool);

export {
  query,
  pool
};
