export default async function handleResponse(query, res) {
  try {
    let json = await query;
    if (json.data) {
      res.status(200).json(json);
    } else if (json.error) {
      let statusCode = json.code ? json.code : 404;
      res.status(statusCode).json(json);
    }
  } catch(err) {
    if (process.env.NODE_ENV !== 'production') {
      console.log(err);
    }

    // In production, log using a service like Sentry
    // ...

    res.status(500).json({
      error: {
        title: 'Internal Server Error'
      }
    });
  }

  res.end();
}
