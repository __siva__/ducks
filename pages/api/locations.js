import { withApiAuthRequired } from '@auth0/nextjs-auth0';

import handleResponse from './_lib/handle-response';
import Location from "./models/locations";
const location = new Location();

export default withApiAuthRequired(async function handler(req, res) {
  if (req.method === 'GET') {
    let { name, country, latitude, longitude } = req.query;
    return handleResponse(location.find({ name, country, latitude, longitude }), res);
  }

  if (req.method === 'POST') {
    let data = req.body.data;
    return handleResponse(location.create(data), res);
  }
});
