import { withApiAuthRequired } from '@auth0/nextjs-auth0';

import handleResponse from '../../_lib/handle-response';
import ensureAdmin from '../../_lib/ensure-admin';
import FeedEvents from '../../models/feed-events';
const feedEvents = new FeedEvents();

export default withApiAuthRequired(async function handler(req, res) {
  if (req.method === 'GET') {
    let handler = ((req, res) => {
      return handleResponse(feedEvents.findAll(req.query), res)
    });

    return ensureAdmin(handler)(req, res);
  }

  if (req.method === 'POST') {
    res.status(405).send({
      error: {
        title: 'Method Not Allowed'
      }
    });

    res.end();
  }
});
