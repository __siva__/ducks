import { withApiAuthRequired } from '@auth0/nextjs-auth0';

import handleResponse from './_lib/handle-response';
import FeedEvents from './models/feed-events';
const feedEvents = new FeedEvents();

export default withApiAuthRequired(async function handler(req, res) {
  if (req.method === 'GET') {
    res.status(405).send({
      error: {
        title: 'Method Not Allowed'
      }
    });

    res.end();
  }

  if (req.method === 'POST') {
    return handleResponse(feedEvents.create(req, res), res);
  }
});
