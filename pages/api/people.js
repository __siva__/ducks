import { withApiAuthRequired } from '@auth0/nextjs-auth0';

import handleResponse from './_lib/handle-response';
import Person from "./models/people";
const person = new Person();

export default withApiAuthRequired(async function handler(req, res) {
  if (req.method === 'GET') {
    return handleResponse(person.find(req.query.email), res);
  }

  if (req.method === 'POST') {
    return handleResponse(person.create({ email: req.body.data.email, name: req.body.data.name }), res);
  }
});
