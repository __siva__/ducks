import { withApiAuthRequired } from '@auth0/nextjs-auth0';

import handleResponse from './_lib/handle-response';
import ensureAdmin from './_lib/ensure-admin';
import FoodCategories from './models/food-categories';
const foodCategories = new FoodCategories();

export default withApiAuthRequired(async function handler(req, res) {
  if (req.method === 'GET') {
    return handleResponse(foodCategories.findAll(), res);
  }

  if (req.method === 'POST') {
    let handler = ((req, res) => {
      return handleResponse(foodCategories.create(req.body.data), res)
    });

    return ensureAdmin(handler)(req, res);
  }
});
