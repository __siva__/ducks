import Head from 'next/head';
import Link from 'next/link';

import { useUser } from '@auth0/nextjs-auth0';

import Loading from '../components/Loading';

function Container(props) {
  return (
    <div className="container">
      <div className="flex flex-col p-10 m-10 ml-16">
        {props.children}
      </div>
    </div>
  );
}

function Title(props) {
  return (
    <Head>
      <title>Hello, {props.name}!</title>
    </Head>
  );
}

export default function Home() {
  let { user, error, isLoading } = useUser();
  if (isLoading) return <Loading />;
  if (error) return <div>{error.message}</div>;

  if (user) {
    return (
      <Container>
        <Title name={user.name} />
        <h1 className="text-xl md:text-7xl break-words font-bold pt-3 pb-5">
          Hello, {user.name}!
        </h1>
        <p className="pt-3 pb-3">
          <Link href="/record">
            <a>Did you feed your ducks today?</a>
          </Link>
        </p>
      </Container>
    );
  }

  return (
    <Container>
      <Title name="stranger" />

      <h1 className="text-7xl font-bold pt-3 pb-5">
        Hello, stranger!
      </h1>
      <p className="pt-3 pb-3">
        <a href="/api/auth/login">Would you like to log in?</a>
      </p>
    </Container>
  );
}
